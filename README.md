## ShelfView ##

**Android custom view to display books on shelf**

<img src="/portrait.png" width="340"> <img src="/landscape.png" width="528">

#### How to use ####
----

**build.gradle**
```
allprojects {
      repositories {
           maven {
               url 'https://jitpack.io'
           }
      }
}
```


```
dependencies {
        implementation 'com.gitlab.joielechong:shelfview:2.3.5'
}
```

**Layout**
```
<com.rilixtech.shelfview.ShelfView
    android:id="@+id/shelfView"
    android:layout_width="match_parent"
    android:layout_height="match_parent" />

```
Attributes

Set the no image found text with `shelfView:shv_no_image_found`, for example:

```
<com.rilixtech.shelfview.ShelfView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:shelfView="http://schemas.android.com/apk/res-auto"
    android:id="@+id/shelfView"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    shelfView:shv_no_image_found="Could Not Found Image"
    />
```

Set title of book as no image placeholder with `shelfView:shv_title_as_placeholder="true"`

**Activity**
```
import com.rilixtech.shelfview.ShelfBook;
import com.rilixtech.shelfview.ShelfView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ShelfView.BookListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ShelfView shelfView = (ShelfView) findViewById(R.id.shelfView);
        shelfView.setBookListener(this);
        List<ShelfBook> books = new ArrayList<>();

        books.add(ShelfBook.createBookWithUrl("http://eurodroid.com/pics/beginning_android_book.jpg", "1", "Beginning Android"));
       
        shelfView.loadBooks(books);
    }

    @Override
    public void onBookClicked(int position, ShelfBook shelfBook) {
       // handle click events here
       //Toast.makeText(this, shelfBook.getBookTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBookLongClicked(int position, ShelfBook shelfBook) {
      // handle click events here
    }
}

```



#### Loading book covers from other sources ####
----

* Internal/External directory in the device
```
books.add(ShelfBook.createBookWithFile("/path/to/android_book_cover.jpg", "1", "Let's Talk About Android"));
shelfView.loadBooks(books);
```

* Assets folder
```
books.add(ShelfBook.createBookWithAsset("android.jpg", "1", "Android for Experts"));
shelfView.loadBooks(books);
```

* Drawable folder
```
books.add(ShelfBook.createBookWithDrawable("alice", "1", "Alice in Wonderland"));
shelfView.loadBooks(books);
``` 

* Drawable id
```
books.add(ShelfBook.createBookWithDrawable(R.drawable.asynchronous_android, "1", "Asynchronous Android"));
shelfView.loadBooks(books);
```

* Raw resource id
```
books.add(ShelfBook.createBookWithRaw(R.raw.android_hacker, "1", "Android Hacker"));

shelfView.loadBooks(books);
```

#### Permissions ####
----
```
   <uses-permission android:name="android.permission.INTERNET" />
   <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
``` 



#### License ####
----
```
Copyright 2018 Joielechong

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```



#### Credits ####
---
* [Picasso](https://github.com/square/picasso)
*[ShelfView](https://github.com/tdscientist/ShelfView)

