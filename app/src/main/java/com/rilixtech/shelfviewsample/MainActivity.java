package com.rilixtech.shelfviewsample;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.rilixtech.shelfview.ShelfBook;
import com.rilixtech.shelfview.ShelfView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ShelfView.BookListener {
  public static final String TAG = "MainActivity";
  private ShelfView shelfView;
  private static int progress = 10;
  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    shelfView = findViewById(R.id.shelfView);
    shelfView.setBookListener(this);
    List<ShelfBook> books = new ArrayList<>();

    books.add(ShelfBook.createBookWithUrl(getString(R.string.url_smashing_android_ui), "1",
        getString(R.string.title_smashing_android_ui), progress));
    books.add(ShelfBook.createBookWithUrl("", "2",
        getString(R.string.title_android_app_dev_for_dummies), 100));
    books.add(ShelfBook.createBookWithUrl(getString(R.string.url_beginning_android), "3",
        getString(R.string.title_beginning_android)));
    books.add(ShelfBook.createBookWithUrl(getString(R.string.url_professional_android_programming), "4",
        getString(R.string.title_professional_android_programming)));
    books.add(
        ShelfBook.createBookWithUrl(getString(R.string.url_android_programming_pshing_the_limits), "5",
        getString(R.string.title_android_programming_pshing_the_limits), 70));
    books.add(ShelfBook.createBookWithUrl(getString(R.string.url_100_q_and_a), "6", getString(R.string.title_100_q_and_a)));
    books.add(ShelfBook.createBookWithUrl(getString(R.string.url_android_programming_for_beginners), "7",
        getString(R.string.title_android_programming_for_beginners)));
    books.add(ShelfBook.createBookWithUrl(getString(R.string.url_android_programming), "8",
        getString(R.string.title_android_programming)));
    books.add(ShelfBook.createBookWithDrawable("cover", "9", "Smashing Magazine", 77));
    books.add(ShelfBook.createBookWithUrl(getString(R.string.url_android_programming), "9", getString(R.string.title_android_programming)));
    books.add(ShelfBook.createBookWithAsset("expert_android_studio.jpg", "10", getString(R.string.title_expert_android_studio)));
    books.add(ShelfBook.createBookWithUrl(getString(R.string.url_android_programming), "11",
        getString(R.string.title_android_programming)));
    books.add(ShelfBook.createBookWithRaw(R.raw.android_hacker, "12", getString(R.string.title_android_hacker)));
    books.add(ShelfBook.createBookWithDrawable(R.drawable.asynchronous_android, "13", getString(R.string.title_android_asynchronous)));

    shelfView.loadBooks(books);
  }

  @Override public void onBookClicked(int position, ShelfBook bookModel) {
    //Toast.makeText(this, bookModel.getTitle(), Toast.LENGTH_SHORT).show();
    //progress = bookModel.getProgress() + 10;
    //shelfView.updateBookProgress(bookModel.getId(), progress);
    Log.d(TAG, "onBookClicked called");
  }

  @Override public void onBookLongClicked(int position, ShelfBook book) {
    Log.d(TAG, "onBookLongClicked called");
  }
}
